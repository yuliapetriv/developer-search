# recruit-hub
## Helper tool for recruitment

### Development
- Navigate to `api` folder by `cd api`.
- Run `npm i` to install `node_modules`.
- Run `npm run db:start` to start the db server if not started yet
- Run `npm run db:create` to create the db if not created yet
- Run `npm run dev` to start the API server.
- Examples of calls are available in `/api/examples.json`.