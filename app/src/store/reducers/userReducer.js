import {
  FETCH_LIST_BEGIN,
  FETCH_LIST_SUCCESS,
  FETCH_LIST_FAILURE
} from '../actions/userListActions';

const initState = {
  items: [],
  page: 0
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_LIST_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        items: state.items.concat(action.payload.users),
        page: action.payload.page
      };
    case FETCH_LIST_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        items: [],
        page: 0
      };
    default:
      return state;
  }
};

export default userReducer;
