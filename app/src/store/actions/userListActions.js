import axios from 'axios';

export const FETCH_LIST_BEGIN = 'FETCH_LIST_BEGIN';
export const FETCH_LIST_SUCCESS = 'FETCH_LIST_SUCCESS';
export const FETCH_LIST_FAILURE = 'FETCH_LIST_FAILURE';

const API_BASE_URL = process.env.API_BASE_URL || 'http://localhost:8081';

const fetchListBegin = () => ({
  type: FETCH_LIST_BEGIN
});

const fetchListSuccess = data => ({
  type: FETCH_LIST_SUCCESS,
  payload: data
});

const fetchListFailure = error => ({
  type: FETCH_LIST_FAILURE,
  payload: error
});

export const fetchUserList = (
  location,
  language,
  frameworks,
  numberOfRepos,
  numberOfFollowers,
  page
) => {
  let q = '';
  if (location) {
    q += `&location=${location}`;
  }
  if (language) {
    q += `&language=${language}`;
  }
  if (frameworks) {
    const arr = frameworks.split(',');
    q+= arr.map(framework => `&frameworks=${framework}`);
  }
  if (numberOfRepos) {
    q += `&numberOfRepositories=${numberOfRepos}`;
  }
  if (numberOfFollowers) {
    q += `&followers=${numberOfFollowers}`;
  }
  if (page) {
    q += `&page=${page}`;
  }
  if (q) {
    q = q.substring(1);
  }
  const url = `${API_BASE_URL}/search?${q}`;
  return dispatch => {
    dispatch(fetchListBegin());
    return axios({
      method: 'GET',
      url,
      headers: {
        'x-api-key': sessionStorage.getItem('github_token')
      }
    })
      .then(response => {
        dispatch(fetchListSuccess(response.data));
        return response.data;
      })
      .catch(error => {
        dispatch(fetchListFailure(error));
      });
  };
};
