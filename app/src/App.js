import React from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './components/Header';
import Home from './components/Home';
import UserDetails from './components/UserDetails';
import SignIn from './components/auth/SignIn';
import AuthCallback from './components/auth/AuthCallback';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Route exact path="/" component={Home} />
        <Route path="/users/:id" component={UserDetails} />
        <Route path="/signin" component={SignIn} />
        <Route path="/auth/callback" component={AuthCallback} />
      </div>
    </BrowserRouter>
  );
}

export default App;
