export const NUMBR_OF_REPOS_OPTIONS = [
  {
    name: 'unset',
    placeholder: 'Кількість репозиторіїв: 0',
    value: '0'
  },
  {
    name: 'minimal',
    placeholder: 'Кількість репозиторіїв: більше 5',
    value: '5'
  },
  {
    name: 'medium',
    placeholder: 'Кількість репозиторіїв: більше 10',
    value: '10'
  }
];

export const NUMBR_OF_FOLLOWERS_OPTIONS = [
  {
    name: 'undef',
    placeholder: 'Кількість підписників: 0',
    value: '0'
  },
  {
    name: 'min',
    placeholder: 'Кількість підписників: більше 5',
    value: '5'
  },
  {
    name: 'med',
    placeholder: 'Кількість підписників: більше 10',
    value: '10'
  }
];
