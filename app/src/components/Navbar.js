import React from 'react';
import '../App.css';
import { NavLink } from 'react-router-dom';
import homeIcon from '../homepage.png';

function Navbar() {
  return (
    <nav className="navbar navbar-expand-md navbar-dark sticky-top">
      <div className="container-fluid">
        <button
          className="navbar-toggler collapsed"
          type="button"
          data-toggle=""
          data-target="#navbarResponsive"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav">
            <NavLink to="/"><img className="icon" src={homeIcon}/></NavLink>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
