import React from 'react';
import '../App.css';
import firstImg from '../paper.jpg';

function Banner() {
  return (
    <div className="banner">
      <h1 className="display-4">Найкраще рішення для спільної розробки</h1>
      <h4>Пошук потенційних кандидатів, контриб'юторів та колабораторів</h4>
      <img src={firstImg} alt="banner" />
      <div className="overlay"></div>
    </div>
  );
}

export default Banner;
