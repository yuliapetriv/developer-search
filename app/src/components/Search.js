import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import '../App.css';
import { connect } from 'react-redux';
import { fetchUserList } from '../store/actions/userListActions';
import { NUMBR_OF_REPOS_OPTIONS } from '../constants';

import { NUMBR_OF_FOLLOWERS_OPTIONS } from '../constants';

class Search extends Component {
  constructor() {
    super();
    this.state = {
      location: '',
      language: '',
      frameworks: '',
      numberOfRepos: NUMBR_OF_REPOS_OPTIONS[0].value,
      numberOfFollowers: NUMBR_OF_FOLLOWERS_OPTIONS[0].value,
      isUsersListRequestSent: false
    };
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  handleSubmit = () => {
    const { location, language, frameworks, numberOfRepos, numberOfFollowers } = this.state;
    this.props
      .fetchUserList(location, language, frameworks, numberOfRepos, numberOfFollowers, this.props.page + 1)
      .then(() => {
        this.setState({
          isUsersListRequestSent: true
        });
      });
  };
  onUserCardClick = id => {
    this.props.history.push(`users/${id}`);
  };
  render() {
    const { items, isLoading } = this.props;

    return (
      <div className="container main_search">
        <form name="search">
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="location"
              value={this.state.location}
              onChange={e => this.handleChange('location', e.target.value)}
            />
            <label className="bmd-label-floating">
              Місцезнаходження (e.g. Kyiv, Ukraine)
            </label>
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="language"
              value={this.state.language}
              onChange={e => this.handleChange('language', e.target.value)}
            />
            <label className="bmd-label-floating">Мова програмування</label>
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="language"
              value={this.state.frameworks}
              onChange={e => this.handleChange('frameworks', e.target.value)}
            />
            <label className="bmd-label-floating">Технології</label>
          </div>
          <div className="form-group repos-group">
            {NUMBR_OF_REPOS_OPTIONS.map(option => (
              <div className="radio">
                <label>
                  <input
                    type="radio"
                    className="repos"
                    name={option.name}
                    value={option.value}
                    checked={option.value === this.state.numberOfRepos}
                    onChange={e =>
                      this.handleChange('numberOfRepos', e.target.value)
                    }
                  />
                  {option.placeholder}
                </label>
              </div>
            ))}
          </div>

          <div className="form-group">
            {NUMBR_OF_FOLLOWERS_OPTIONS.map(option => (
              <div className="radio">
                <label>
                  <input
                    type="radio"
                    className="followers"
                    name={option.name}
                    value={option.value}
                    checked={option.value === this.state.numberOfFollowers}
                    onChange={e =>
                      this.handleChange('numberOfFollowers', e.target.value)
                    }
                  />
                  {option.placeholder}
                </label>
              </div>
            ))}
          </div>

          {!items.length && (
            <div className="search-btn-wrapper">
              <button
                type="button"
                className="btn btn-primary btn-raised"
                onClick={this.handleSubmit}
              >
                Пошук
              </button>
            </div>
          )}
        </form>
        <div className="container">
          <div className="row">
            <Fragment>
              {!!items.length &&
                items.map(item => {
                  return (
                    <div
                      className="card col-md-4 col-sm-6"
                      key={item.id}
                      onClick={() => this.onUserCardClick(item.login)}
                    >
                      <img
                        className="card-img-top"
                        src={item.avatar_url}
                        alt="ava"
                      />
                      <div className="card-body">
                        <h5 className="card-title">{item.login}</h5>
                        <a
                          className="card-text"
                          target="_blank"
                          href={item.html_url}
                        >
                          Сторінка на GitHub
                        </a>
                      </div>
                    </div>
                  );
                })}
              {!!(!items.length && this.state.isUsersListRequestSent) && (
                <div className="response_nothing">Нічого не знайдено :(</div>
              )}
              {!!(items.length && this.state.isUsersListRequestSent) && (
                <div className="search-btn-wrapper">
                  <button
                    type="button"
                    className="btn btn-primary btn-raised"
                    onClick={this.handleSubmit}
                  >
                    Завантажити більше
                  </button>
                </div>
              )}
            </Fragment>
          </div>
          {isLoading && (
            <div className="loading">
              <div className="fancy-spinner">
                <div className="ring"></div>
                <div className="ring"></div>
                <div className="dot"></div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.user.items || [],
    page: state.user.page || 0,
    isLoading: state.user.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchUserList: (...args) => dispatch(fetchUserList(...args))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Search));
