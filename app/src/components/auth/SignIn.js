import React, { Component } from 'react';

class SignIn extends Component {
  state = {
    username: ''
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    window.location = `https://github.com/login/oauth/authorize?client_id=62a4a34c009c7f9993f9&redirect_uri=http://localhost:3000/auth/callback&login=${this.state.username}`;
  };
  render() {
    return (
      <div className="container sign-in-header">
        <form onSubmit={this.handleSubmit}>
          <h5>Вхід</h5>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              id="username"
              name="username"
              onChange={this.handleChange}
            />
            <label className="bmd-label-floating">GitHub username</label>
          </div>
          <button
            type="submit"
            className="btn btn-primary btn-raised"
            onClick={this.handleSubmit}
          >
            Увійти
          </button>
        </form>
      </div>
    );
  }
}

export default SignIn;
