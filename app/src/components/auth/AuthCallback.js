import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../App.css';
import { connect } from 'react-redux';
import axios from 'axios';

const API_BASE_URL = process.env.API_BASE_URL || 'http://localhost:8081';

class AuthCallback extends Component {
  componentDidMount() {
    const code = this.props.location.search.replace('?code=', '');
    if (!code) {
      throw new Error('missing code');
    }
    return axios({
      method: 'GET',
      url: `${API_BASE_URL}/auth/token?code=${code}`
    })
      .then(response => {
        sessionStorage.setItem('github_token', response.data);
        this.props.history.push('/');
      })
      .catch(error => {
        console.error(error);
        alert('Auth failed');
      });
  }
  render() {
    return null;
  }
}

export default connect(
  null,
  null
)(withRouter(AuthCallback));
