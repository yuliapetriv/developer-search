import React from 'react';
import '../App.css';
import { NavLink } from 'react-router-dom';
import Navbar from './Navbar';

function Header() {
  return (
    <header>
      <div className="header">
        <Navbar />
      </div>
    </header>
  );
}

export default Header;
