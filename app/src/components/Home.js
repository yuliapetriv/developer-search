import React from 'react';
import '../App.css';
import Search from './Search';
import Banner from './Banner';

class Home extends React.Component {
  componentDidMount() {
    const token = sessionStorage.getItem('github_token');
    if (token) {
      return;
    }
    this.props.history.push('/signin');
  }
  render() {
    return (
      <div>
        <Banner />
        <Search />
      </div>
    );
  }
}

export default Home;
