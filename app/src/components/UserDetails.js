import React from 'react';
import locationIcon from '../maps-and-flags.png';
import emailIcon from '../black-back-closed-envelope-shape.png';
import groupIcon from '../group.png';
import linkIcon from '../link.png';
import phoneIcon from '../telephone.png';
import githubIcon from '../github-sign.png';
import bioIcon from '../quotes.png';
import axios from 'axios';

class UserDetails extends React.Component {
    state = {
        user: [],
        org: []
    }
    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`https://api.github.com/users/${id}`)
            .then(res => {
                this.setState({
                    user: res.data
                })
            })      
    }

    componentDidUpdate () {}
    render () {
        const id = this.props.match.params.id;
        const { user } = this.state;
        return (
            <div className="user-page">
                <div className="container">
                    <div className="row">
                        <div className="card col-md-4">
                            <img className="card-img-top" src={user.avatar_url} />
                            <div className="card-body">                            
                                <h3 className="card-text">{user.name}</h3>
                                <p className="card-title">{id}</p>
                                <div className="icon-wrap"><img className="icon"src={githubIcon}/><a href={user.html_url} target="_blank">Профіль на GitHub</a></div>
                                <p className="card-title icon-wrap"><img className="icon" src={bioIcon}/>{user.bio}</p>
                                <div className="icon-wrap"><img className="icon" src={locationIcon}/>{user.location}</div>
                                <div className="icon-wrap"><img className="icon" src={groupIcon}/>{user.company}</div>
                                <div className="icon-wrap"><img className="icon"src={emailIcon}/><a href={user.email}>{user.email}</a></div>                                
                                <div className="icon-wrap"><img className="icon" src={linkIcon}/><a rel="nofollow me" href={user.blog} target="_blank">{user.blog}</a></div>
                                <div className="icon-wrap"><img className="icon" src={phoneIcon}/><a href=""></a></div>

                            </div>
                        </div>
                        <div className="col-md-8">
                        <div className="container">
                            <div className="row">
                                <div className="col heading">Кількість репозиторіїв</div>
                                <div className="col heading">Кількість публічних комітів</div>
                                                            
                            </div>
                            <div className="row table-indent">
                                <div className="col table-data">{user.public_repos}</div>
                                <div className="col table-data">{user.public_gists}</div>                           
                            </div>

                            <div className="row">                            
                                <div className="col heading">Followers</div>
                                <div className="col heading">Following</div>
                                                        
                            </div>
                            <div className="row table-indent">
                                <div className="col table-data">{user.followers}</div>
                                <div className="col table-data">{user.following}</div>
                            </div>

                            <div className="row">
                                <div className="col heading">Кількість зірок</div>
                                <div className="col heading">Кількість проектів</div>
                            </div>
                            <div className="row table-indent">
                                <div className="col table-data"></div>
                                <div className="col table-data"></div>
                            </div>

                            <div className="row">
                                <div className="col heading">Організації</div> 
                                <div className="col heading">Мови програмування</div>
                            </div>
                            <div className="row table-indent">
                                <div className="col table-data"></div>
                                <div className="col table-data"></div>
                            </div>

                            <div className="row">
                                <div className="col heading">Технології</div>
                                <div className="col heading">Остання активність</div>                         
                            </div>
                            <div className="row table-indent">
                                <div className="col table-data">Angular</div>
                                <div className="col table-data">{user.updated_at}</div>
                            </div>
                        </div>    
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
    

export default UserDetails;