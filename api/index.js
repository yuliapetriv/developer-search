const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const search = require('./routes/search');
const users = require('./routes/users');
const auth = require('./routes/auth');

const app = express();
app.use(compression());
app.use(cors());
app.use(bodyParser.json());

app.use('/auth', auth);
app.use('/search', search);
app.use('/users', users);

app.use('*', (req, res, next) => {
  let err = new Error('Page Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  const status = err && err.status;
  const message = err && err.message;
  res.status(status || 500).send(message || 'Something went wrong');
});

const port = process.env.PORT || 8081;
app.listen(port, () => {
  console.log(`Server started at port: ${port}`);
});
