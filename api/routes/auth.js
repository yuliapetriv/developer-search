const Router = require('express').Router;
const asyncMiddleware = require('../middlewares/asyncMiddleware');
const axios = require('../utils/axios');
const { github } = require('../config/endpoints');
const router = Router();

router.get(
  '/token',
  asyncMiddleware(async (request, response) => {
    const code = request.query.code;
    const result = await axios({
      method: 'POST',
      url: `https://github.com/login/oauth/access_token`,
      data: {
        client_id: github.clientID,
        client_secret: process.env.GITHUB_CLIENT_SECRET,
        code
      }
    });
    if (result.data.startsWith('error')) {
      return response.status(400).send(result.data);
    }
    response.send(
      result.data
        .replace('access_token=', '')
        .replace('&scope=&token_type=bearer', '')
    );
  })
);

module.exports = router;
