const Router = require('express').Router;
const asyncMiddleware = require('../middlewares/asyncMiddleware');
const axios = require('../utils/axios');
const { github } = require('../config/endpoints');
const schema = require('../schemas/search');
const { buildQueryString } = require('../utils/gitHub');
const router = Router();

router.get(
  '/:id',
  asyncMiddleware(async (request, response) => {
    const id = request.params.id;
    const result = await axios({
      method: 'GET',
      url: `${github.baseURL}/users/${id}`
    });
    response.json({
      ...result.data
    });
  })
);

module.exports = router;
