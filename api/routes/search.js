const Router = require('express').Router;
const asyncMiddleware = require('../middlewares/asyncMiddleware');
const search = require('../lib/search');
const schema = require('../schemas/search');
const router = Router();

router.get(
  '/',
  asyncMiddleware(async (request, response) => {
    const query = request.query;
    let data;
    try {
      data = await schema.validateAsync(query);
    } catch (err) {
      console.error(err);
      return response.json(err);
    }
    if (data.frameworks && typeof data.frameworks === "string") {
      data.frameworks = [data.frameworks];
    }
    const token = request.headers['x-api-key'];
    const result = await search(data, token);
    response.json(result);
  })
);

module.exports = router;
