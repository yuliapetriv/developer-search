module.exports = {
  buildQueryString({
    language,
    location,
    numberOfRepositories,
    followers,
    nickname,
    name,
    email
  }) {
    let queryString = 'type:user';
    if (language) {
      queryString += `+language:${language}`;
    }
    if (location) {
      queryString += `+location:${location}`;
    }
    if (numberOfRepositories) {
      queryString += `+repos:>=${numberOfRepositories}`;
    }
    if (followers) {
      queryString += `+followers:>=${followers}`;
    }
    if (nickname) {
      queryString += `+user:${nickname}`;
    }
    if (name) {
      queryString += `+${name}+in:name`;
    }
    if (email) {
      queryString += `+${email}+in:email`;
    }
    return queryString;
  }
};
