const axios = require('axios');

const axiosInstance = axios.create({
  validateStatus: status => {
    return status >= 200 && status < 400;
  }
});

module.exports = axiosInstance;
