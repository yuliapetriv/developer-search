const axios = require('axios');
const { github } = require('../config/endpoints');
const db = require('../database');
const { UserFrameworks } = db;
const Op = db.Sequelize.Op;

module.exports = async function userHasReposWithFrameworks(user, frameworks) {
  const dbQuery = {
    where: {
      userId: user.login,
      framework: { [Op.or]: frameworks }
    }
  };
  const dbUserFrameworks = await UserFrameworks.findAll(dbQuery);
  if (dbUserFrameworks.length === frameworks.length) {
    return !dbUserFrameworks.find(dbF => dbF.dataValues.isFamiliar === false);
  }

  const ghTasks = frameworks.map(framework => {
    if (
      dbUserFrameworks.find(
        dbUserFramework => dbUserFramework.dataValues.framework === framework
      )
    ) {
      return Promise.resolve({
        data: {
          total_count: 1
        }
      });
    }
    return axios({
      method: 'GET',
      url: `${github.baseURL}/search/repositories?per_page=1&q=${framework}+in:readme+user:${user.login}`
    });
  });

  const result = await Promise.all(ghTasks);

  let response = true;
  const createTasks = [];
  result.forEach((ghFramework, i) => {
    const isFamiliar = ghFramework.data.total_count > 0;
    if (!isFamiliar) {
      response = false;
    }
    if (
      dbUserFrameworks.find(
        dbUserFramework =>
          dbUserFramework.dataValues.framework === frameworks[i]
      )
    ) {
      return;
    }
    createTasks.push(
      UserFrameworks.create({
        userId: user.login,
        framework: frameworks[i],
        isFamiliar
      })
    );
  });

  await Promise.all(createTasks);
  return response;
};
