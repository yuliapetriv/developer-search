const axios = require('axios');
const { buildQueryString } = require('../utils/gitHub');
const { github } = require('../config/endpoints');
const userHasReposWithFrameworks = require('./userHasReposWithFrameworks');

const PAGE_SIZE = 6;

module.exports = async function search(query, token, prevRes = { users: [] }) {
  const page = query.page || 1;
  const queryString = buildQueryString(query);
  const result = await axios({
    method: 'GET',
    url: `${github.baseURL}/search/users?per_page=${PAGE_SIZE}&page=${page}&sort=repositories&q=${queryString}`,
    headers: token
      ? {
          Authorization: `token ${token}`
        }
      : {}
  });
  const users = result.data.items;
  console.log(page);
  console.log(users.map(user => user.login))
  if (!users.length) {
    return {
      users: prevRes.users.concat(users),
      page
    };
  }
  if (!query.frameworks || !query.frameworks.length) {
    return {
      users: prevRes.users.concat(users),
      page
    };
  }
  const reposTasks = users.map(user => {
    return userHasReposWithFrameworks(user, query.frameworks);
  });
  let userFrameworks;
  try {
    userFrameworks = await Promise.all(reposTasks);
  } catch (err) {
    console.error(err);
    return {
      users: prevRes.users,
      page: (page - 1) || 1
    };
  }
  const filtered = users.filter((user, index) => userFrameworks[index]);
  if (
    filtered.length >= PAGE_SIZE - prevRes.users.length ||
    result.data.total_count <= PAGE_SIZE * page
  ) {
    const usersWithFrameworks = prevRes.users.concat(filtered).slice(0, 6)
    return {
      users: usersWithFrameworks,
      page
    };
  }
  return search({ ...query, page: page + 1 }, token, {
    users: prevRes.users.concat(filtered)
  });
};

//C:\Program Files\PostgreSQL\11\bin>