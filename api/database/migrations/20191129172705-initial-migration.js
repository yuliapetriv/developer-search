module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('UserFrameworks', {
        id: {
          type: Sequelize.UUID,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
          allowNull: false,
          autoIncrement: false
        },
        userId: {
          type: Sequelize.STRING,
          allowNull: false
        },
        framework: {
          type: Sequelize.STRING,
          allowNull: false
        },
        isFamiliar: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
      .then(() =>
        queryInterface.addIndex('UserFrameworks', ['userId', 'framework'])
      );
  }
};
