module.exports = (sequelize, DataTypes) =>
  sequelize.define('UserFrameworks', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    userId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    framework: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isFamiliar: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  });
