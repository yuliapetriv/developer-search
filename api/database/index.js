const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const config = require('./config.js')[process.env.NODE_ENV || 'development'];

const sequelize = new Sequelize(config);

const db = {};
const modelsPath = path.join(__dirname, '/models');
fs.readdirSync(modelsPath).forEach(file => {
  const model = sequelize.import(path.join(modelsPath, file));
  db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
