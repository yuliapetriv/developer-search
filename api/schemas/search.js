const Joi = require('@hapi/joi');
const languages = require('../constants/languages');

module.exports = Joi.object({
  page: Joi.number().integer(),
  language: Joi.string().valid.apply(
    Joi,
    languages.map(language => language.toLowerCase())
  ),
  location: Joi.string()
    .min(1)
    .max(100),
  numberOfRepositories: Joi.number()
    .integer()
    .min(0)
    .max(1000000000),
  followers: Joi.number()
    .integer()
    .min(0)
    .max(1000000000),
  nickname: Joi.string()
    .min(1)
    .max(200),
  name: Joi.string()
    .min(1)
    .max(200),
  email: Joi.string()
    .min(1)
    .max(200),
  frameworks: Joi.alternatives().try(
    Joi.array().items(
      Joi.string()
        .min(1)
        .max(200)
    ),
    Joi.string()
      .min(1)
      .max(200)
  )
}).min(1);
